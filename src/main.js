import Vue from 'vue'
import App from './App.vue'
import store from './store'
// import axios from './request/http.js'
import { Image as VanImage, Lazyload, Circle } from 'vant'

Vue.use(VanImage).use(Lazyload).use(Circle)
Vue.config.productionTip = false

new Vue({
  store,
  render: (h) => h(App)
}).$mount('#app')
