import request from './http.js'

//获取歌单歌曲
export const getsongAPI = function (id, limit, offset) {
    return request.get('/playlist/track/all', {
      params: {
        id,
        limit,
        offset
      }
    })
  },
  //获取歌曲详情
  getsongdeAPI = function (ids) {
    return request.get('/song/detail', {
      params: {
        ids
      }
    })
  },
  //   获取评论
  gethotcommentAPI = function (id, type, limit) {
    return request.get('/comment/hot', {
      params: {
        id,
        type,
        limit
      }
    })
  },
  //   获取歌曲链接
  getsongurlAPI = function (id) {
    return request.get('/song/url', {
      params: { id }
    })
  }
